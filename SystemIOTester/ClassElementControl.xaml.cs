﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Xceed.Wpf.Toolkit;

namespace SystemIOTester
{
    /// <summary>
    /// Interaction logic for ClassElementsControl.xaml
    /// </summary>
    public abstract partial class ClassElementControl : UserControl
    {
        public Dictionary<string, Func<object>> Bindings { get; set; }
        private Dictionary<Type, Func<Type, string, UIElement>> uiPicker;

        public ClassElementControl()
        {
            InitializeComponent();

            this.uiPicker = new Dictionary<Type, Func<Type, string, UIElement>>
            {
                { typeof(String), CreateTextBox },
                { typeof(Boolean), CreateCheckBox },
                { typeof(DateTime), CreateCalendar },
                { typeof(byte[]), CreateTextBlock },
                { typeof(Enum), CreateComboBox },
                { typeof(Encoding), CreateEncodeBox },
                { typeof(int), CreateNumericTextBox },
                { typeof(long), CreateNumericTextBox },
                { typeof(byte), CreateUpDown }
            };
            
            this.Bindings = new Dictionary<string, Func<object>>();
        }

        protected void RenderParameters(dynamic element)
        {
            int i = 0;
            foreach (var parameter in element.GetParameters())
            {
                var label = new Label { Content = "Parameter: " + parameter.Name };
                var type = parameter.ParameterType.IsEnum ? typeof(Enum) : parameter.ParameterType;
                if (!uiPicker.ContainsKey(type))
                    continue;
                var uiElement = uiPicker[type](parameter.ParameterType, parameter.Name);
                Grid.SetColumn(label, 0);
                Grid.SetColumn(uiElement, 1);
                Grid.SetRow(label, i);
                Grid.SetRow(uiElement, i);
                classElementParameters.Children.Add(label);
                classElementParameters.Children.Add(uiElement);
                i++;
            }
        }

        private UIElement CreateCheckBox(Type type, string name)
        {
            var checkbox = new CheckBox { Margin = new Thickness(0, 7, 0, 0), Content = "set true" };
            Bindings[name] = () => checkbox.IsChecked;
            return checkbox;
        }

        protected virtual UIElement CreateTextBox(Type type, string name)
        {
            var stackPanel = new StackPanel { Orientation = Orientation.Horizontal };
            var textbox = new TextBox { Width = 150, MaxLength = 150, MaxLines = 1 };
            Bindings[name] = () => textbox.Text;
            stackPanel.Children.Add(textbox);
            return stackPanel;
        }

        private UIElement CreateCalendar(Type type, string name)
        {
            var calendar = new Calendar();
            Bindings[name] = () => calendar.SelectedDate;
            return calendar;
        }

        private UIElement CreateTextBlock(Type type, string name)
        {
            var stackPanel = new StackPanel { Orientation = Orientation.Horizontal };
            var textblock = new TextBlock { Width = 150, Text = "No bytes", Padding = new Thickness(0, 5, 0, 0) };
            Bindings[name] = () => textblock.Tag as byte[];
            stackPanel.Children.Add(textblock);
            var button = new Button { Content = "Insert Bytes" };
            button.Click += (s, e) =>
            {
                var dialog = new OpenFileDialog();
                if (dialog.ShowDialog().HasValue)
                {
                    textblock.Tag = System.IO.File.ReadAllBytes(dialog.FileName);
                    textblock.Text = "Bytes read";
                }
            };
            stackPanel.Children.Add(button);
            return stackPanel;
        }

        private UIElement CreateComboBox(Type type, string name)
        {
            var combo = new ComboBox();
            Bindings[name] = () => (combo.SelectedItem as ComboBoxItem).Tag;
            foreach (var item in Enum.GetValues(type))
            {
                combo.Items.Add(new ComboBoxItem { Content = item.ToString(), Tag = item });
            }
            combo.SelectedIndex = 0;
            return combo;
        }

        private UIElement CreateEncodeBox(Type type, string name)
        {
            var combo = new ComboBox();
            Bindings[name] = () => Encoding.GetEncoding(combo.SelectedItem as string);
            foreach (var item in Encoding.GetEncodings())
            {
                combo.Items.Add(item.Name);
            }
            combo.SelectedIndex = 0;
            return combo;
        }

        private UIElement CreateNumericTextBox(Type type, string name)
        {
            var stackPanel = new StackPanel { Orientation = Orientation.Horizontal };
            var textbox = new TextBox { Width = 30, MaxLength = 30, MaxLines = 1, Tag = type };
            Bindings[name] = () =>
            {
                if (textbox.Tag.Equals(typeof(Int32)))
                {
                    int result;
                    return int.TryParse(textbox.Text, out result) ? result : -1;
                }
                else
                {
                    long result;
                    return long.TryParse(textbox.Text, out result) ? result : -1;
                }
            };
            stackPanel.Children.Add(textbox);
            return stackPanel;
        }

        private UIElement CreateUpDown(Type type, string name)
        {
            var updown = new IntegerUpDown { Minimum = byte.MinValue, Maximum = byte.MaxValue, Width = 60, HorizontalAlignment = System.Windows.HorizontalAlignment.Left, Value = 0 };
            Bindings[name] = () => (byte)updown.Value;
            return updown;
        }
    }
}
