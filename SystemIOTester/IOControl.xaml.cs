﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

namespace SystemIOTester
{
    /// <summary>
    /// Interaction logic for File.xaml
    /// </summary>
    public partial class IOControl : UserControl
    {
        private MethodControl methodControl;
        private ConstructorControl constructorControl;
        private bool isDisposable;

        private ComboBox constructorBox;
        private object currentStreamConstructor;

        public IOControl(Type type)
        {
            isDisposable = type.GetInterface("IDisposable") != null;
            InitializeComponent();
            InitializeComboBox(type);
            InitializeConstructor(type);
            methodsBox.SelectionChanged += methodsBox_SelectionChanged;
        }

        public MethodInfo GetCurrentMethod()
        {
            return methodControl.Method;
        }

        public object GetCurrentConstructor()
        {
            if (constructorControl == null) return null;
            if (isDisposable) return GetCurrentStreamConstructor();
            return constructorControl.Constructor.Invoke(constructorControl.Bindings.Select(kv => kv.Value()).ToArray<object>());
        }

        public object[] GetParameters()
        {
            return methodControl.Bindings.Select(kv => kv.Value()).ToArray<object>();
        }

        public void Refresh()
        {
            if (isDisposable)
            {
                if (methodControl.Method.Name != "Dispose" && methodControl.Method.Name != "Close")
                    container.Children.Remove(constructorControl);
                else
                {
                    RenderConstructorEnvironment((constructorBox.SelectedItem as ComboBoxItem).Tag as ConstructorInfo);
                    currentStreamConstructor = null;
                }
            }
        }

        private object GetCurrentStreamConstructor()
        {
            if (currentStreamConstructor == null)
                currentStreamConstructor = constructorControl.Constructor.Invoke(constructorControl.Bindings.Select(kv => kv.Value()).ToArray<object>());
            return currentStreamConstructor;
        }

        void methodsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var method = (methodsBox.SelectedItem as ComboBoxItem).Tag;
            RenderMethodEnvironment(method as MethodInfo);
        }

        private void RenderMethodEnvironment(MethodInfo method)
        {
            if (methodControl != null)
                container.Children.Remove(methodControl);
            methodControl = new MethodControl(method);
            Grid.SetRow(methodControl, 3);
            container.Children.Add(methodControl);
        }

        private void InitializeComboBox(Type type)
        {
            foreach (var item in type.GetMethods())
            {
                methodsBox.Items.Add(new ComboBoxItem { Content = item.Name, Tag = item });
            }
            methodsBox.SelectedIndex = 0;
            RenderMethodEnvironment((methodsBox.SelectedItem as ComboBoxItem).Tag as MethodInfo);
        }

        private void InitializeConstructor(Type type)
        {
            var constructors = type.GetConstructors();
            if (!type.IsAbstract)
            {
                constructorBox = CreateConstructorPanel(constructors);
                constructorBox.SelectedIndex = 0;
                RenderConstructorEnvironment((constructorBox.SelectedItem as ComboBoxItem).Tag as ConstructorInfo);
            }
        }

        private void RenderConstructorEnvironment(ConstructorInfo constructor)
        {
            if (constructorControl != null)
                container.Children.Remove(constructorControl);
            constructorControl = new ConstructorControl(constructor);
            Grid.SetRow(constructorControl, 1);
            container.Children.Add(constructorControl);
        }

        private ComboBox CreateConstructorPanel(ConstructorInfo[] constructors)
        {
            var panel = new StackPanel { Orientation = Orientation.Horizontal, Height = 27, VerticalAlignment = System.Windows.VerticalAlignment.Top };
            panel.Children.Add(new Label { Content = "Constructors:" });
            var combo = new ComboBox();
            foreach (var constructor in constructors)
            {
                combo.Items.Add(new ComboBoxItem { Content = constructor.Name, Tag = constructor });
            }
            panel.Children.Add(combo);
            combo.SelectionChanged += (s, e) => RenderConstructorEnvironment((combo.SelectedItem as ComboBoxItem).Tag as ConstructorInfo);
            container.Children.Add(panel);
            return combo;
        }
    }
}
