﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace SystemIOTester
{
    class MethodControl : ClassElementControl
    {
        public MethodInfo Method;

        public MethodControl(MethodInfo method)
        {
            Method = method;

            RenderParameters(Method);

            classElementType.Content = "Method:";
            classElementSignature.Text = Method.ToString();
        }

        protected override UIElement CreateTextBox(Type type, string name)
        {
            var stackPanel = base.CreateTextBox(type, name) as StackPanel;
            var button = new Button { Content = "Insert String", IsEnabled = (name == "contents") };
            button.Click += (s, e) =>
            {
                var dialog = new OpenFileDialog();
                if (dialog.ShowDialog().HasValue)
                    (stackPanel.Children[0] as TextBox).Text = System.IO.File.ReadAllText(dialog.FileName);
            };
            stackPanel.Children.Add(button);
            return stackPanel;
        }
    }
}
