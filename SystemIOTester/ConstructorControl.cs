﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace SystemIOTester
{
    public class ConstructorControl : ClassElementControl
    {
        public ConstructorInfo Constructor;

        public ConstructorControl(ConstructorInfo constructor)
        {
            Constructor = constructor;

            RenderParameters(Constructor);

            classElementType.Content = "Constructor:";
            classElementSignature.Text = Constructor.ToString();
        }
    }
}
