﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.IO;

namespace SystemIOTester
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            tabControl.Items.Add(new TabItem { Header = "File", Content = new IOControl(typeof(File)) });
            tabControl.Items.Add(new TabItem { Header = "Directory", Content = new IOControl(typeof(Directory)) });
            tabControl.Items.Add(new TabItem { Header = "FileInfo", Content = new IOControl(typeof(FileInfo)) });
            tabControl.Items.Add(new TabItem { Header = "DirectoryInfo", Content = new IOControl(typeof(DirectoryInfo)) });
            tabControl.Items.Add(new TabItem { Header = "FileStream", Content = new IOControl(typeof(FileStream)) });
        }

        private void executeBtn_Click(object sender, RoutedEventArgs e)
        {
            var control = tabControl.SelectedContent as IOControl;
            var method = control.GetCurrentMethod();
            var parameters = control.GetParameters();
            try
            {
                var constructor = control.GetCurrentConstructor();
                if (method.ReturnParameter.ParameterType != typeof(void))
                    outputTestBlock.Text = method.Invoke(constructor, parameters).ToString();
                else
                {
                    method.Invoke(constructor, parameters);
                    outputTestBlock.Text = "Method ran succesfully";
                }
                control.Refresh();
            }
            catch (Exception ex)
            {
                outputTestBlock.Text = ex.ToString();
            }
        }
    }
}
